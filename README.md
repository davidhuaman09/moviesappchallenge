# MovieApp

MovieApp es una aplicacion de peliculas desarrollado con swift.

## IMPORTANTE
Para la revision del reto revisar el branch "master"


## Documentación
Ingresar al siguiente link para la revision del [Documento de Evidencias](https://drive.google.com/drive/folders/1np11PJUfLmMYHoh8R9TcLcwyXovemKqV?usp=sharing) 


## Prototipos
Ingresar al siguiente link para la revision del [Prototipo del proyecto](https://xd.adobe.com/view/5e5871eb-8219-4d0d-bf93-1b518431f51c-0b0b/?fullscreen&hints=off) 
