//
//  NetworkManagerConnection.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import RxSwift


class NetworkManagerConnection {
    
    func authUser(user: String, pass: String) -> Observable<Bool> {
        
        return Observable.create { observer in
            
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: Constants.BaseUrl.urlHeroku + Constants.Endpoints.loginEndpoint)!)
            print(Constants.BaseUrl.urlHeroku + Constants.Endpoints.loginEndpoint)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let userAuthRequest = UserAuthRequest(login: user, password: pass)
            request.httpBody = try? JSONEncoder().encode(userAuthRequest)
            
            
            session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else { return }
                
                print(response.statusCode)
                
                if response.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let authResponse = try decoder.decode(UserAuth.self
                            , from: data)
                        print(authResponse.userLogged)
                        observer.onNext(authResponse.userLogged)
                    } catch let error{
                        observer.onError(error)
                        print("Ha ocurrido un error: \(error.localizedDescription)")
                    }
                }
                else if response.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
        
    }
    
    
    
    func getUncommingMovies(page : Int) -> Observable<[Movie]> {
        
        return Observable.create { observer in
            
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: "\(Constants.BaseUrl.urlMovieDB + Constants.Endpoints.listUpcommingMoviesEndpoint)?page=\(page)&api_key=\(Constants.apiKey)")!)
            print("https://api.themoviedb.org/3/movie/upcoming?page=\(page)&api_key=f46b58478f489737ad5a4651a4b25079")
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let movies = try decoder.decode(MoviesResult.self, from: data)
                        
                        observer.onNext(movies.listOfMovies)
                    } catch let error{
                        observer.onError(error)
                        print("Ha ocurrido un error: \(error.localizedDescription)")
                    }
                }
                else if response.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
        
    }
    
    
    
    func getDetailMovies(movideID: String) -> Observable<MovieDetail> {
        return Observable.create { observer in
            
            let session = URLSession.shared
            var request = URLRequest(url: URL(string: "\(Constants.BaseUrl.urlMovieDB)/\(movideID)?api_key=\(Constants.apiKey)" )!)
            print("\(Constants.BaseUrl.urlMovieDB)/\(movideID)&api_key=\(Constants.apiKey)" )
            request.httpMethod = "GET"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            session.dataTask(with: request) { (data, response, error) in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else { return }
                
                if response.statusCode == 200 {
                    do {
                        let decoder = JSONDecoder()
                        let detailMovie = try decoder.decode(MovieDetail.self, from: data)
                        observer.onNext(detailMovie)
                    } catch let error{
                        observer.onError(error)
                        print("Ha ocurrido un error: \(error.localizedDescription)")
                    }
                }
                else if response.statusCode == 401 {
                    print("Error 401")
                }
                observer.onCompleted()
            }.resume()
            
            return Disposables.create {
                session.finishTasksAndInvalidate()
            }
        }
    }
    
    
    func getImageFromServer(urlString: String) -> Observable<UIImage> {
      
        return Observable.create { observer in
            
            let placeHolderImage = UIImage(named: "noposteravailable")!
              print("getimage from server \(urlString)")
            URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in
                
                guard let data = data, error == nil, let response = response as? HTTPURLResponse else { return }
                
                if error != nil {
                    print(error ?? "No Error")
                    return
                }
                
                if response.statusCode == 200 {
                    guard let image = UIImage(data: data) else { return }
                    observer.onNext(image)
                } else {
                    observer.onNext(placeHolderImage)
                }
                //MARK: observer onCompleted event
                observer.onCompleted()
                
            }).resume()
            
            //MARK: return our disposable
            return Disposables.create {}
        }
    }
    
    
    
}
