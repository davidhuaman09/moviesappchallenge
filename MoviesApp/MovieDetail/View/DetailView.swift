//
//  DetailView.swift
//  TheMoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import UIKit
import RxSwift

class DetailView: UIViewController {
    
    @IBOutlet private weak var titleHeader: UILabel!
    @IBOutlet private weak var imageFilm: UIImageView!
    @IBOutlet private weak var descriptionMovie: UILabel!
    @IBOutlet private weak var releaseDate: UILabel!

    @IBOutlet weak var originalLanguage: UILabel!
    @IBOutlet private weak var voteAverage: UILabel!
    
    private var router = DetailRouter()
    private var viewModel = DetailViewModel()
    private var disposeBag = DisposeBag()
    var movieID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataAndShowDetailMove()
        viewModel.bind(view: self, router: router)
    }
    
    private func getDataAndShowDetailMove() {
        guard let idMovie = movieID else { return }
        return viewModel.getMovieData(movieID: idMovie).subscribe(
            onNext: { movie in
                self.showMovieData(movie: movie)
        },
            onError: { error in
                print("Ha ocurrido un error: \(error)")
        },
            onCompleted: {
        }).disposed(by: disposeBag)
    }
    
    private func getImageMovie(movie: MovieDetail) {
        return viewModel.getImageMovie(urlString: Constants.BaseUrl.urlImage + movie.backDropPath)
            .subscribe(
                onNext: { image in
                    DispatchQueue.main.sync {
                        self.imageFilm.image = image
                    }
            },
                onError: { error in
                    print(error.localizedDescription)
            },
                onCompleted: {
            }).disposed(by: disposeBag)
    }
    
    func showMovieData(movie: MovieDetail) {
        DispatchQueue.main.async {
            self.titleHeader.text = movie.title
            self.descriptionMovie.text = movie.overview
            self.releaseDate.text = movie.releaseDate
            self.originalLanguage.text = movie.originalLanguage.uppercased()
            self.voteAverage.text = String(movie.voteAverage)
            self.getImageMovie(movie: movie)
        }
    }
}
