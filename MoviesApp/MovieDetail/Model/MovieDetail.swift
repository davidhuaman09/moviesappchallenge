//
//  MovieDetail.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation

struct MovieDetail: Codable {
    let backDropPath : String
    let title: String
    let voteAverage : Double
    let releaseDate : String
    let originalLanguage : String
    let overview : String
    
    enum CodingKeys: String, CodingKey {
        case backDropPath = "backdrop_path"
        case  title
        case voteAverage = "vote_average"
        case releaseDate = "release_date"
        case originalLanguage = "original_language"
        case overview
        
    }
    
}
