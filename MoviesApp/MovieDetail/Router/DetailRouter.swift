//
//  DetailRouter.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import UIKit

class DetailRouter {
    var viewController: UIViewController {
        return createViewController()
    }
    
    var movieID: String?
    
    private var sourceView: UIViewController?
    
    init(movieID: String? = "") {
        self.movieID = movieID
    }
    
    func setSourceView(_ sourceView: UIViewController?) {
        guard let view = sourceView else {fatalError("Error desconocido")}
        self.sourceView = view
    }
    
    private func createViewController() -> UIViewController {
        let view = DetailView(nibName: "DetailView", bundle: Bundle.main)
        view.movieID = self.movieID
        return view
    }
}
