//
//  DetailViewModel.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import RxSwift

class DetailViewModel {
    private var managerConnections = NetworkManagerConnection()
    private(set) weak var view: DetailView?
    private var router: DetailRouter?
    
    func bind(view: DetailView, router: DetailRouter) {
        self.view = view
        self.router = router
        self.router?.setSourceView(view)
    }
    
    func getMovieData(movieID: String) -> Observable<MovieDetail> {
        return managerConnections.getDetailMovies(movideID: movieID)
    }
    
    func getImageMovie(urlString: String) -> Observable<UIImage> {
        return managerConnections.getImageFromServer(urlString: urlString)
    }
}
