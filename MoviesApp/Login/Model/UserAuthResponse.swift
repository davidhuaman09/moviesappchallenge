//
//  UserAuth.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation

struct UserAuth : Codable{
    let userLogged: Bool
    let result: Result
    enum CodingKeys: String, CodingKey {
        case userLogged
        case result = "resultado"
    }
}
struct Result: Codable{
    let code: String
    let description: String
    enum CodingKeys: String, CodingKey {
        case code = "codigo"
        case description = "descripcion"
    }
}
