//
//  UserAuthRequest.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation


final class UserAuthRequest: Codable {
    let login: String
    let password: String
    init(login: String, password: String) {
        self.login = login
        self.password = password
    }
}
