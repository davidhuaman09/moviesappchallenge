//
//  LoginViewModel.swift
//  MoviesApp
//
//  Created by David Gomez on 20/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class LoginViewModel {
    
      private var networkManagerConnection = NetworkManagerConnection()
      private(set) weak var view: LoginView?
      private var router: LoginRouter?
    
       let usernameTextPublishSubject =  PublishSubject<String>()
       let passwordTextPublishSubject =  	PublishSubject<String>()
    
      func bind(view: LoginView, router: LoginRouter) {
          self.view = view
          self.router = router
          self.router?.setSourceView(view)
      }
    
    
    		
    func validateCredentials(user: String, pass: String) -> Observable<Bool>{
        return networkManagerConnection.authUser(user: user, pass: pass)
    }
      
    func enableFields() -> Observable<Bool>{
        return Observable.combineLatest(usernameTextPublishSubject.asObservable().startWith(""), passwordTextPublishSubject.asObservable().startWith("")).map{ username, password in
            return username.count > 3 && password.count > 3
        }.startWith(false)
    }
    
    
    func makeHomeView() {
        router?.navigateHomeView()
    }
    
    
}
