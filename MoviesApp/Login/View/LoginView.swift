//
//  LoginView.swift
//  MoviesApp
//
//  Created by David Gomez on 20/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginView: UIViewController {

    
    private var router = LoginRouter()
    private var viewModel = LoginViewModel()
    private var disposeBag = DisposeBag()
    
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    private var userValid = false
    
    
    @IBOutlet weak var loadSpinner: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        loadSpinner.isHidden = true
        viewModel.bind(view: self, router: router)
        
        userTextField.rx.text.map{ $0 ?? ""}.bind(to: viewModel.usernameTextPublishSubject).disposed(by: disposeBag)
        passwordTextField.rx.text.map{ $0 ?? ""}.bind(to: viewModel.passwordTextPublishSubject).disposed(by: disposeBag)
        viewModel.enableFields().bind(to: enterButton.rx.isEnabled).disposed(by: disposeBag)
        viewModel.enableFields().map{ $0 ? 1 : 0.5}.bind(to: enterButton.rx.alpha).disposed(by: disposeBag)
        
      
    }

    @IBAction func enterBtnTapped(_ sender: Any) {
        loadSpinner.isHidden = false
        loadSpinner.startAnimating()
        authenticateUser(user: userTextField.text!, pass: passwordTextField.text!)
           
        
    }
    
    private func authenticateUser(user: String, pass: String){
        
        return viewModel.validateCredentials(user: user, pass: pass)
            .subscribe(on: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            //Suscríbirme a el observable
                .subscribe(
                    onNext: { validation in
                        self.showHomeView(authResul: validation)
                        
                }, onError: { error in
                    print(error.localizedDescription)
                     self.showHomeView(authResul: false)
                }, onCompleted: {
                }).disposed(by: disposeBag)
            //Dar por completado la secuencia de RxSwift
      
    }
    
    private func showHomeView(authResul : Bool){
        if authResul {
            viewModel.makeHomeView()
        }else{
             showMessage(title: "Alerta", message: "Usuario o contrasena incorrectos", messageInButton: "Intentar de nuevo")
        }
        loadSpinner.isHidden =  true
        loadSpinner.stopAnimating()
    }
    
    private func showMessage(title: String, message: String, messageInButton: String){
        openAlert(title: title, message: message, alertStyle: .alert, actionTitles: ["Okay"], actionStyles: [.default], actions: [{ _ in
                                           print(messageInButton)
                                       }])
    }
    
}
