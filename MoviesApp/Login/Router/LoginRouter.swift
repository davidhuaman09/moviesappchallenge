//
//  LoginRouter.swift
//  MoviesApp
//
//  Created by David Gomez on 20/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import UIKit

class LoginRouter {
    
    var viewController: UIViewController{
        return createViewController()
    }
    
    private var sourceView: UIViewController?
    
    private func createViewController() -> UIViewController{
           let view = LoginView(nibName: "LoginView", bundle: Bundle.main)
           return view
       }
    
    func setSourceView(_ sourceView : UIViewController?){
         guard let view = sourceView else {
             fatalError("Unknown Error")
         }
         self.sourceView = view
     }
    
    func navigateHomeView() {
        let homeView = HomeRouter().viewController
        sourceView?.navigationController?.pushViewController(homeView, animated: true)
        homeView.navigationItem.hidesBackButton = true
       }
    
    
}
