//
//  HomeRouter.swift
//  MoviesApp
//
//  Created by David Gomez on 20/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import UIKit


class HomeRouter {
    var viewController: UIViewController{
        return createViewController()
    }
    
    private var sourceView: UIViewController?
    
    private func createViewController() -> UIViewController{
        
        let view = HomeView(nibName: "HomeView", bundle: Bundle.main)
        return view
    }
    
    func setSourceView(_ sourceView : UIViewController?){
        guard let view = sourceView else {
            fatalError("Unknown Error")
        }
        
        self.sourceView = view
    }
    
    func navigateToDetailView(movieID: String) {
        let detailView = DetailRouter(movieID: movieID).viewController
        sourceView?.navigationController?.pushViewController(detailView, animated: true)
    }
    
}
