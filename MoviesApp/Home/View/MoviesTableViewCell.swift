//
//  MoviesTableViewCell.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import UIKit

class MoviesTableViewCell: UITableViewCell {

    @IBOutlet weak var movieBackdropIv: UIImageView!
    @IBOutlet weak var movieTitlelbl: UILabel!
    @IBOutlet weak var movieDescriptionlbl: UILabel!
    @IBOutlet weak var movieRatinglbl: UILabel!
    
    
    @IBOutlet weak var movieIdiomlbl: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
