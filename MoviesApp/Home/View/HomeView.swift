//
//  HomeView.swift
//  MovieAppChallenge
//
//  Created by David Gomez on 19/02/21.
//

import UIKit
import RxSwift

class HomeView: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadMoreMoviesBtn: UIButton!
   
    @IBOutlet weak var chargeSpinner: UIActivityIndicatorView!
    
    
    private var router = HomeRouter()
    private var viewModel = HomeViewModel()
    private var disposeBag = DisposeBag()
    private var movies = [Movie]()
    private var filteredMovies = [Movie]()
    var currentPage = 1
    
    lazy var searchController: UISearchController = ({
        let controller = UISearchController(searchResultsController: nil)
        controller.hidesNavigationBarDuringPresentation = true
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.sizeToFit()
        controller.searchBar.barStyle = .default
        controller.searchBar.backgroundColor = .clear
        controller.searchBar.placeholder = "Buscar una pélicula"
        
        return controller
    })()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Proximas Peliculas"
        loadMoreMoviesBtn.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 45)
        loadMoreMoviesBtn.setTitle("Cargas mas peliculas", for: .normal)
        
        configureTableView()
        viewModel.bind(view: self, router: router)
        getData(page: currentPage )
        manageSearchBarController()
        
    }
    
    
    @IBAction func loadMoreMoviesBtnTapped(_ sender: UIButton) {
        currentPage += 1
        getData(page: currentPage)
        
    }
    
    private func configureTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UINib(nibName: "MoviesTableViewCell", bundle: nil), forCellReuseIdentifier: "MoviesTableViewCell")
    }
    
    private func getData(page: Int) {
        return viewModel.getListMoviesData(page: page)
            //Manejar la concurrencia o hilos de RxSwift
            .subscribe(on: MainScheduler.instance)
            .observe(on: MainScheduler.instance)
            //Suscríbirme a el observable
            .subscribe(
                onNext: { movies in
                    if !movies.isEmpty {
                        self.chargeSpinner.stopAnimating()
                        self.chargeSpinner.isHidden = true
                        self.movies.append(contentsOf: movies)
                        self.reloadTableView()
                    }else{
                         self.tableView.tableFooterView?.isHidden = true
                    }
                    
            }, onError: { error in
                print(error.localizedDescription)
            }, onCompleted: {
            }).disposed(by: disposeBag)
        //Dar por completado la secuencia de RxSwift
    }
    
    private func reloadTableView() {
        DispatchQueue.main.async {
            //self.activity.stopAnimating()
            //self.activity.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    
    private func manageSearchBarController() {
        let searchBar = searchController.searchBar
        
        searchController.delegate = self
        tableView.tableHeaderView = searchBar
        tableView.contentOffset = CGPoint(x: 0, y: searchBar.frame.size.height)
        
        searchBar.rx.text
            .orEmpty
            .distinctUntilChanged()
            .subscribe(onNext: { (result) in
                self.filteredMovies = self.movies.filter({ movie in
                    self.reloadTableView()
                    return movie.title.contains(result)
                })
            })
            .disposed(by: disposeBag)
    }
}

extension HomeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return self.filteredMovies.count
        }
        else {
            return self.movies.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoviesTableViewCell") as! MoviesTableViewCell
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            if self.filteredMovies[indexPath.row].posterImage != nil{
                cell.movieBackdropIv.loadimageIntoIvFromUrl(urlString: "\(Constants.BaseUrl.urlImage+self.filteredMovies[indexPath.row].posterImage!)", placeHolderImage: UIImage(named: "noposteravailable")!)
            }else{
                cell.movieBackdropIv.loadimageIntoIvFromUrl(urlString: "\(Constants.BaseUrl.urlImage)", placeHolderImage: UIImage(named: "noposteravailable")!)
            }
            cell.movieTitlelbl.text = filteredMovies[indexPath.row].title
            cell.movieDescriptionlbl.text = filteredMovies[indexPath.row].originalTile
        }
        else {
            if self.movies[indexPath.row].posterImage != nil{
                cell.movieBackdropIv.loadimageIntoIvFromUrl(urlString: "\(Constants.BaseUrl.urlImage+self.movies[indexPath.row].posterImage!)", placeHolderImage: UIImage(named: "noposteravailable")!)
            }else{
                cell.movieBackdropIv.loadimageIntoIvFromUrl(urlString: "\(Constants.BaseUrl.urlImage)", placeHolderImage: UIImage(named: "noposteravailable")!)
            }
            
            
            cell.movieTitlelbl.text = movies[indexPath.row].title
            cell.movieDescriptionlbl.text = movies[indexPath.row].overview
            cell.movieRatinglbl.text = "\(movies[indexPath.row].voteAverage) / 10"
            cell.movieIdiomlbl.text = movies[indexPath.row].originalLanguage.uppercased()
           
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       if searchController.isActive && searchController.searchBar.text != "" {
        viewModel.makeDetailView(movieID: String(self.filteredMovies[indexPath.row].movieId))
              }
              else {
        viewModel.makeDetailView(movieID: String(self.movies[indexPath.row].movieId))
              }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        
        if indexPath.section == lastSectionIndex  && indexPath.row == lastRowIndex{
            self.tableView.tableFooterView = loadMoreMoviesBtn
            self.tableView.tableFooterView?.isHidden = false
            
        }
    }
    

}

extension HomeView: UISearchControllerDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.isActive = false
        reloadTableView()
    }
}


