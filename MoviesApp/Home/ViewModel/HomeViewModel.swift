//
//  HomeViewModel.swift
//  MoviesApp
//
//  Created by David Gomez on 20/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation
import RxSwift


class HomeViewModel {
    private weak var view: HomeView?
    private var router: HomeRouter?
    private var networkmanagerConnection = NetworkManagerConnection()

    func bind(view: HomeView, router: HomeRouter){
        self.view = view
        self.router = router
        
        //binding del router con la vista
        self.router?.setSourceView(view)
        
    }
    
    func getListMoviesData(page: Int) -> Observable<[Movie]>{
        return networkmanagerConnection.getUncommingMovies(page: page)
    }
    
    func makeDetailView(movieID: String) {
         router?.navigateToDetailView(movieID: movieID)
     }

}


