//
//  Movie.swift
//  MovieAppChallenge
//
//  Created by David Gomez on 19/02/21.
//

import Foundation


struct MoviesResult: Codable{
    let listOfMovies: [Movie]
    enum CodingKeys: String, CodingKey {
        case listOfMovies = "results"
    }
    
}

	
struct Movie: Codable{
    let movieId: Int
    let originalTile: String
    let title: String
    let overview: String
    let popularity: Double
    let posterImage: String?
    let voteAverage: Double
    let originalLanguage: String
    
    enum CodingKeys: String, CodingKey {
        case movieId = "id"
        case originalTile = "original_title"
        case title
        case overview
        case popularity
        case posterImage = "poster_path"
        case voteAverage = "vote_average"
        case originalLanguage = "original_language"
    }
}
