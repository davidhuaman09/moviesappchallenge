//
//  Constants.swift
//  MoviesApp
//
//  Created by David Gomez on 21/02/21.
//  Copyright © 2021 David Gomez. All rights reserved.
//

import Foundation



struct Constants {
    
    static let apiKey = "f46b58478f489737ad5a4651a4b25079"
    
    struct BaseUrl {
        static let urlMovieDB = "https://api.themoviedb.org/3/movie"
        static let urlImage = "https://image.tmdb.org/t/p/w500"
        static let urlHeroku = "https://appmovies09.herokuapp.com"
    }
    
    struct Endpoints {
        static let loginEndpoint = "/movieapp/validateUser"
        static let listUpcommingMoviesEndpoint = "/upcoming"
    }
    
}
